import os
from argparse import ArgumentParser, Action, ArgumentDefaultsHelpFormatter, SUPPRESS, Namespace
from io import BytesIO
import mimetypes
from pprint import pprint
from tokenize import Name

from yaml import parse
import PyPDF2
import pdf2image
import sheatless
import magic

from tests.sometest import sometest
from tests.contentbox import contentbox

os.umask(0) # Simplifies management stuff like deleting output files from the code editor on the host system.


def find_file_paths(base_directory, allowed_extensions, case_sensitive_extensions=False):
	"""
	Returns a list of paths to all files with an allowed extension in base_directory

	Example:
	-----------------------
	+-- input_pdfs
	|   +-- foo.pdf
	|   +-- bar.PDF
	|   +-- baz.png
	-----------------------
	findFilePaths("input_pdfs", [".pdf"])
	=> ["input_pdfs/foo.pdf", "input_pdfs/bar.PDF"]
	"""
	if not case_sensitive_extensions: allowed_extensions = [ext.lower() for ext in allowed_extensions]
	res = []
	for (dirpath, dirnames, filenames) in os.walk(base_directory):
		for filename in filenames:
			basename, extension = os.path.splitext(filename)
			if not case_sensitive_extensions: extension = extension.lower()
			if (extension in allowed_extensions):
				res.append(os.path.join(dirpath, filename))
	return res

def clear_dir(directory, recursive=True):
	for (dirpath, dirnames, filenames) in os.walk(directory):
		for filename in filenames:
			os.remove(os.path.join(directory, filename))
		if recursive:
			for dirname in dirnames:
				clear_dir(os.path.join(directory, dirname))
				os.rmdir(os.path.join(directory, dirname))

def tuplify_function_return(ret):
	if ret is None:
		return ()
	if type(ret) is not tuple:
		return (ret,)
	return ret


INPUT_PDF_DIR = "input_pdfs"
OUTPUT_PDF_DIR = "output_pdfs"
INPUT_IMG_DIR = "input_images"
OUTPUT_IMG_DIR = "output_images"
TMP_PATH = "tmp"

if not os.path.exists(INPUT_PDF_DIR):     os.mkdir(INPUT_PDF_DIR)
if not os.path.exists(OUTPUT_PDF_DIR):    os.mkdir(OUTPUT_PDF_DIR)
if not os.path.exists(INPUT_IMG_DIR):     os.mkdir(INPUT_IMG_DIR)
if not os.path.exists(OUTPUT_IMG_DIR):    os.mkdir(OUTPUT_IMG_DIR)
if not os.path.exists(TMP_PATH):          os.mkdir(TMP_PATH)

clear_dir(TMP_PATH)



class clear_output_action(Action):
	def __init__(self, option_strings, dest, **kwargs):
		return super().__init__(option_strings, dest, nargs=0, default=SUPPRESS, **kwargs)
	
	def __call__(self, parser, namespace, values, option_string, **kwargs):
		clear_dir(OUTPUT_IMG_DIR)
		clear_dir(OUTPUT_PDF_DIR)


def get_input_pdf_paths(base_directory_or_pdf_path):
	if os.path.isfile(base_directory_or_pdf_path):
		return [base_directory_or_pdf_path]
	return find_file_paths(base_directory_or_pdf_path, [".pdf"])

def get_intput_image_paths(base_directory_or_img_path):
	if os.path.isfile(base_directory_or_img_path):
		return [base_directory_or_img_path]
	return find_file_paths(base_directory_or_img_path, [".png", ".jpg", ".jpeg", ".gif", ".bmp"])

argument_parser_formatter = lambda prog: ArgumentDefaultsHelpFormatter(prog, max_help_position=50)


class SheatlessDevArguments:
	def get_page_numbers(self, pdf_number_of_pages):
		page_numbers = range(pdf_number_of_pages)
		if self.pages not in ["all", ["all"]]:
			page_numbers = [(int(page_nr) - 1) for page_nr in self.pages]
		return page_numbers

	def open_pdf(self, pdf_path):
		pdf = PyPDF2.PdfFileReader(pdf_path)
		page_numbers = self.get_page_numbers(pdf.getNumPages())
		pdf_writer = PyPDF2.PdfFileWriter()
		for page_number in page_numbers:
			try:
				page = pdf.getPage(page_number)
			except:
				print(f"WARNING: Page number {page_number + 1} does not exist in {pdf_path}.")
				continue
			pdf_writer.addPage(page)
		pdf_bytes = BytesIO()
		pdf_writer.write(pdf_bytes)
		pdf_bytes.seek(0)
		return pdf_bytes
	
	def get_input_files(self):
		match self.input_type:
			case "pdf":
				pdf_paths = get_input_pdf_paths(self.input)
				for pdf_path in pdf_paths:
					pdf = self.open_pdf(pdf_path)
					yield pdf_path, pdf
			case "image":
				image_paths = get_intput_image_paths(self.input)
				for image_path in image_paths:
					with open(image_path, "rb") as image:
						yield image_path, image


class SheatlessDevArgumentParser(ArgumentParser):
	def __init__(self, **kwargs):
		super().__init__(description="Develop and test sheatless", formatter_class=argument_parser_formatter, **kwargs)
		self.add_argument("--clear-output", action=clear_output_action, help="Clear output directories first.")
		self.add_argument("--input-type", type=str, choices=["image", "pdf"], default="pdf", help="Select input type.")
		self.add_argument("input", type=str, nargs="?", default="./", help="Path to input file or directory.")
		self.add_argument("pages", type=str, nargs="*", default="all", help="Select which pages to analyze.")
	
	def parse_args(self, *args, **kwargs):
		return super().parse_args(*args, namespace=SheatlessDevArguments(), **kwargs)
	
