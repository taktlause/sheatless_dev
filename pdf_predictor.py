from common import SheatlessDevArgumentParser
from pprint import pprint

from sheatless import PdfPredictor

argument_parser = SheatlessDevArgumentParser()
arguments = argument_parser.parse_args()

# Override input type, just in case
arguments.input_type = "pdf"

for path, file in arguments.get_input_files():
    print()
    print(f"Running PdfPredictor for {path}:")
    pdf_predictor = PdfPredictor(file.read(), crop_to_top=True)
    parts = list(pdf_predictor.parts())
    print("parts:", end=" ")
    pprint(parts, sort_dicts=False)
