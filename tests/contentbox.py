import os
import io
import yaml
import sheatless
from PIL import Image
import numpy as np


def contentbox(img : io.BytesIO, engine_kwargs : dict):
    img_array = np.asarray(Image.open(img))
    x, y, width, height, histogram_array, img_filtered_array, img_filtered_chart_array = sheatless.findContentBox(img_array)

    print("x: {}, y: {}, width: {}, height: {}".format(x, y, width, height))

    histogram = io.BytesIO()
    img_filtered = io.BytesIO()
    img_filtered_chart = io.BytesIO()

    Image.fromarray(histogram_array).save(histogram, format="png")
    Image.fromarray(img_filtered_array).convert("RGB").save(img_filtered, format="png")
    Image.fromarray(img_filtered_chart_array).convert("RGB").save(img_filtered_chart, format="png")

    return ["0riginal", img], ["histogram", histogram], ["img_filterd", img_filtered], ["img_filtered_chart", img_filtered_chart]
    img = engine.cropImage(img)
    config = "--user-words sheetmusicUploader/instrumentsToLookFor.txt --psm 11 --dpi 96 -l eng"
    config += " --oem 1"
    config += " --tessdata-dir \"{}\"".format(TESSDATA_DIR)
    detectionData = pytesseract.image_to_data(img, output_type=pytesseract.Output.DICT, config=config)
