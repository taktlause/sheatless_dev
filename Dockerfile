FROM python:3.10

# ensure realtime prints to terminal
ENV PYTHONUNBUFFERED=1

# set a directory for the app
WORKDIR /app

# download data set
RUN mkdir tessdata
RUN wget -O tessdata/tessdata_best.zip https://github.com/tesseract-ocr/tessdata_best/archive/refs/tags/4.1.0.zip --progress=dot:giga
RUN unzip tessdata/tessdata_best.zip -d tessdata/
RUN rm tessdata/tessdata_best.zip

# install apt packages
RUN apt-get update
RUN apt-get -y install ffmpeg libsm6 libxext6 poppler-utils tesseract-ocr libtesseract-dev libleptonica-dev pkg-config

# install dependencies
COPY requirements.txt requirements.txt
RUN pip install --no-cache-dir -r requirements.txt

# install sheatless with dependencies
ARG SHEATLESS_PIP="no"
COPY sheatless_full_repo sheatless_full_repo
RUN if [ $SHEATLESS_PIP = yes ]; then pip install  ./sheatless_full_repo; else pip install -r sheatless_full_repo/requirements.txt; fi
