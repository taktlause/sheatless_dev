# Sheatless development

## Overview

The sheatless development package is a handy interface/framework for developing sheatless.

It is recommended to use docker since the project has collected quite a bunch of dependencies and docker makes the installation process so much more automagic.

## Build docker container

```
docker-compose build
```

## Enter docker container

```
docker-compose run app bash
```

## Usage

The entry point is `main.py`, which uses argparse to generate a flexible CLI. The full synopsis for this interface is

```
python main.py [-h] [--clear-output] [--engine ENGINE] [--tessdata-dir TESSDATA_DIR] operation {img,pdf} [input] [pages ...]
```

where the second positional argument is `input_type`. `input` is relative path from `input_pdfs` or `input_images` to the file or directory you want to analyze. `input` can be skipped, then the script will take all the files it finds. If `input` is a directory, the script will take all files recursively in that directory. If `input_type` is `pdf`, you can also specify which pages you want to analyze. If no pages are provided all pages will be analyzed. `operation` is the name of the python function you want to perform on each pdf page or image. That function should have the following interface:

```py
import io
def operation(img: io.BytesIO, engine_kwargs: dict):
    ...
    return ["identifier_1", io.BytesIO(output_img_1)], ...
```

As we can see the function must accept one input image and a dictionary of engine kwargs, and can return any number of output images. Image format is same as input image when `input_type=img`, and png when `input_type=pdf`. All output images will then be stored in `output_images/`. The operation function must also accept arguments from argparse as keywordarguments.

You can get a more detailed description of the arguments by running the help command

```
python main.py -h
```

There is also a way to clear the output directories:

```
python main.py --clear-output
```

## Example usage

Given you have a function called `blur` like this:

```py
import io
from PIL import Image
import numpy as np

def blur(img, engine_kwargs):
    pixel_array = np.asarray(Image.open(img))
    np.blur(pixel_array) # Not sure if blur is a numpy function though...
    ret = io.BytesIO()
    Image.fromarrray(pixel_array).save(ret, format="png")
    return ["blurred", ret]
```

and the following file structure:

```
+- input_pdfs
|  +- a.pdf
|  +- b.pdf
|  +- c
|  |  +- e.pdf
|  |  +- f.pdf
+- input_images
|  +- g.png
|  +- h.png
```

, here is some commands you might want to run:

Execute `blur` on all pages in `a.pdf`:

```
python main.py blur pdf a.pdf
```

Execute `blur` on all pages in all pdfs:

```
python main.py blur pdf
```

Execute `blur` on all pages in all pdfs the `c` directory:

```
python main.py blur pdf c
```

Execute `blur` on page 2 and 3 in `a.pdf`:

```
python main.py blur pdf a.pdf 2 3
```
Execute `blur` on all pdfs, but clear old output data first:

```
python main.py --clear-output blur pdf
```

Execute `blur` on all images:

```
python main.py blur img
```

The format for specifying an image file or directory is the same as for pdfs. The `--clear-output` flag of course works for images as well.

It is not possible to operate on images in the `input_pdfs` folder or pdfs in the `input_images` folder.

# Sheatless build and deployment

## Build sheatless package

```
docker-compose run app sh build_package.sh
```

## Deploy shealess package

This requires you to configure an API token in your `~/.pypirc`. To do that log in as thebeatless [here](https://pypi.org/manage/account/) and add a token for sheatless and add it to `~/.pypirc`.

It also requires you to install twine, and I do not encourage doing this in docker as I think it will be a mess, and not really that useful.

```
pip install --upgrade twine
```

And then the actual deployment command is

```
python3 -m twine upload sheatless_full_repo/dist/*
```
