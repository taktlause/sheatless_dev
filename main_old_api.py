import os
import argparse
from io import BytesIO
import mimetypes
from pprint import pprint

from yaml import parse
import PyPDF2
import pdf2image
import sheatless
import magic

from tests.sometest import sometest
from tests.contentbox import contentbox

os.umask(0) # Simplifies management stuff like deleting output files from the code editor on the host system.


def findFilePaths(base_directory, allowed_extensions, case_sensitive_extensions=False):
	"""
	Returns a list of paths to all files with an allowed extension in base_directory

	Example:
	-----------------------
	+-- input_pdfs
	|   +-- foo.pdf
	|   +-- bar.PDF
	|   +-- baz.png
	-----------------------
	findFilePaths("input_pdfs", [".pdf"])
	=> ["input_pdfs/foo.pdf", "input_pdfs/bar.PDF"]
	"""
	if not case_sensitive_extensions: allowed_extensions = [ext.lower() for ext in allowed_extensions]
	res = []
	for (dirpath, dirnames, filenames) in os.walk(base_directory):
		for filename in filenames:
			basename, extension = os.path.splitext(filename)
			if not case_sensitive_extensions: extension = extension.lower()
			if (extension in allowed_extensions):
				res.append(os.path.join(dirpath, filename))
	return res

def clear_dir(directory, recursive=True):
	for (dirpath, dirnames, filenames) in os.walk(directory):
		for filename in filenames:
			os.remove(os.path.join(directory, filename))
		if recursive:
			for dirname in dirnames:
				clear_dir(os.path.join(directory, dirname))
				os.rmdir(os.path.join(directory, dirname))

def tuplify_function_return(ret):
	if ret is None:
		return ()
	if type(ret) is not tuple:
		return (ret,)
	return ret


INPUT_PDF_DIR = "input_pdfs"
OUTPUT_PDF_DIR = "output_pdfs"
INPUT_IMG_DIR = "input_images"
OUTPUT_IMG_DIR = "output_images"
TMP_PATH = "tmp"

if not os.path.exists(INPUT_PDF_DIR):     os.mkdir(INPUT_PDF_DIR)
if not os.path.exists(OUTPUT_PDF_DIR):    os.mkdir(OUTPUT_PDF_DIR)
if not os.path.exists(INPUT_IMG_DIR):     os.mkdir(INPUT_IMG_DIR)
if not os.path.exists(OUTPUT_IMG_DIR):    os.mkdir(OUTPUT_IMG_DIR)
if not os.path.exists(TMP_PATH):          os.mkdir(TMP_PATH)

clear_dir(TMP_PATH)



class clear_output_action(argparse.Action):
	def __init__(self, option_strings, dest, **kwargs):
		return super().__init__(option_strings, dest, nargs=0, default=argparse.SUPPRESS, **kwargs)
	
	def __call__(self, parser, namespace, values, option_string, **kwargs):
		clear_dir(OUTPUT_IMG_DIR)
		clear_dir(OUTPUT_PDF_DIR)

formatter = lambda prog: argparse.ArgumentDefaultsHelpFormatter(prog, max_help_position=50)
parser = argparse.ArgumentParser(description="Develop and test sheatless", formatter_class=formatter)
parser.add_argument("--clear-output", action=clear_output_action, help="Clear output directories first")
parser.add_argument("operation", type=str, help="Name of python function to execute on each pdf page or image. The function recieves a bytes-like image and can return a list of tuples (descriptor, byte-like image) that should be outputted")
parser.add_argument("input_type", type=str, choices=["img", "pdf"], help="Select input type")
parser.add_argument("input", type=str, nargs="?", default="./", help="Path to input file or directory relative to input_images or input_pdfs.")
parser.add_argument("pages", type=str, nargs="*", default="all", help="Select which pages to analyze")
parser.add_argument("--engine", type=str, default="lstm", help="Options: lstm, legacy.")
parser.add_argument("--tessdata-dir", type=str, default="tessdata/tessdata_best-4.1.0/", help="Sets the TESSDATA directory for tesseract.")
args = parser.parse_args()

engine_kwargs = {
	"use_lstm": True if args.engine == "lstm" else False,
	"tessdata_dir": args.tessdata_dir,
}

if args.input_type == "img":
	raise Exception("input_type img not supported in old API")
		
elif args.input_type == "pdf":
	if os.path.isfile(os.path.join(INPUT_PDF_DIR, args.input)):
		pdf_paths = [os.path.join(INPUT_PDF_DIR, args.input)]
	else:
		pdf_paths = findFilePaths(os.path.join(INPUT_PDF_DIR, args.input), [".pdf"])

	for pdf_path in pdf_paths:
		# Strip away INPUT_PDF_DIR and file extension from pdf_path
		output_path_small = os.path.join(*os.path.splitext(pdf_path)[0].split("/")[1:])
		# Join with OUTPUT_IMG_DIR to get path to output image directory for this pdf
		output_path = os.path.join(OUTPUT_IMG_DIR, output_path_small)
		if not os.path.exists(output_path): os.makedirs(output_path)

		parts, instrumentsDefaultParts = sheatless.processUploadedPdf(pdf_path, output_path, **engine_kwargs)
		print("parts:", end=" ")
		pprint(parts)
		print("instrumentsDefaultParts:", end=" ")
		pprint(instrumentsDefaultParts)

		print("and with the slightly newer API:")
		with open(pdf_path, "rb") as pdf_file:
			parts, instrumentsDefaultParts = sheatless.predict_parts_in_pdf(pdf_file.read(), **engine_kwargs)
			print("parts:", end=" ")
			pprint(parts)
			print("instrumentsDefaultParts:", end=" ")
			pprint(instrumentsDefaultParts)

		print("and with the even newer PdfPredictor-API:")
		with open(pdf_path, "rb") as pdf_file:
			pdfPredictor = sheatless.PdfPredictor(pdf_file.read(), crop_to_top=True, **engine_kwargs)
			for partDict in pdfPredictor.parts():
				print("partDict:", end=" ")
				pprint(partDict, sort_dicts=False)

else:
	raise Exception(f"input_type {args.input_type} not recognized.")

